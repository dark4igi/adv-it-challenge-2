resource "aws_iam_policy" "startstop" {
  name = "startstop"
  path = "/"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:DescribeInstances",
          "ec2:StartInstances",
          "ec2:StopInstances",
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role" "startstop" {
  name = "startstop"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
  managed_policy_arns = [aws_iam_policy.startstop.arn]
}

data "archive_file" "stop_zip" {
  type        = "zip"
  source_file = "./files/stop.py"
  output_path = "./stop.zip"

}

resource "aws_lambda_function" "stop" {
  depends_on = [
    data.archive_file.stop_zip
  ]
  filename         = "stop.zip"
  function_name    = "stop"
  role             = aws_iam_role.startstop.arn
  handler          = "stop.lambda_handler"
  source_code_hash = filebase64sha256("./stop.zip")

  runtime = "python3.9"
  timeout = 300

  environment {
    variables = {
      tags = join("", [trimsuffix("[%{for tag in var.tags}{\"${tag.key}\":\"${tag.value}\"},%{endfor}", ","), "]"])
    }
  }
}

data "archive_file" "start_zip" {
  type        = "zip"
  source_file = "./files/start.py"
  output_path = "./start.zip"

}

resource "aws_lambda_function" "start" {
  depends_on = [
    data.archive_file.start_zip
  ]
  filename         = "start.zip"
  function_name    = "start"
  role             = aws_iam_role.startstop.arn
  handler          = "start.lambda_handler"
  source_code_hash = filebase64sha256("./start.zip")

  runtime = "python3.9"
  timeout = 300

  environment {
    variables = {
      tags = join("", [trimsuffix("[%{for tag in var.tags}{\"${tag.key}\":\"${tag.value}\"},%{endfor}", ","), "]"])
    }
  }
}

resource "aws_cloudwatch_event_rule" "start" {
  name        = "start"
  description = "start ec2 "

  schedule_expression = "cron(${var.start_time.m} ${var.start_time.h} ? * ${var.start_time.d} *)"
}

resource "aws_cloudwatch_event_target" "start" {
  rule      = aws_cloudwatch_event_rule.start.name
  target_id = "StartInstance"
  arn       = aws_lambda_function.start.arn
}

resource "aws_lambda_permission" "start" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = aws_cloudwatch_event_rule.start.name
  principal = "events.amazonaws.com"
  source_arn = aws_cloudwatch_event_rule.start.arn
}


resource "aws_cloudwatch_event_rule" "stop" {
  name        = "stop"
  description = "stop ec2 "

  schedule_expression = "cron(${var.stop_time.m} ${var.stop_time.h} ? * ${var.stop_time.d} *)"
}

resource "aws_cloudwatch_event_target" "stop" {
  rule      = aws_cloudwatch_event_rule.stop.name
  target_id = "StopInstance"
  arn       = aws_lambda_function.stop.arn
}

resource "aws_lambda_permission" "stop" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = aws_cloudwatch_event_rule.stop.name
  principal = "events.amazonaws.com"
  source_arn = aws_cloudwatch_event_rule.stop.arn
}
