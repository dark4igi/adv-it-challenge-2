resource "aws_security_group" "http_and_ssh" {
  name   = "http_and_ssh"
  vpc_id = data.aws_vpc.default.id

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }


}


resource "random_shuffle" "subnets_enabled" {
  input        = data.aws_subnet_ids.subnets.ids
  result_count = var.enabled_count
}



resource "aws_instance" "servers_enabled" {
  count         = var.enabled_count
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id     = random_shuffle.subnets_enabled.result[count.index]
  tags = merge (local.tags,{
    Name      = "server-enabled-${count.index}"
  })
}

resource "random_shuffle" "subnets_disabled" {
  input        = data.aws_subnet_ids.subnets.ids
  result_count = var.disabled_count
}



resource "aws_instance" "servers_disavled" {
  count         = var.disabled_count
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id     = random_shuffle.subnets_disabled.result[count.index]
  tags = {
    startstop = "disabled"
    Name      = "server-disabled-${count.index}"
  }
}
