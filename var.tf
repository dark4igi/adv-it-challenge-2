variable "enabled_count" {
  default = 4
}

variable "disabled_count" {
  default = 2
}

variable "tags" {
  default = [
    {
      "key" : "startstop",
      "value" : "enabled"
    },
    {
      "key" : "foo",
      "value" : "bar"
    },
    {
      "key": "super"
      "value": "cool"
    }
  ]
}

variable "start_time" {
  default = {
    h = 8
    m = 0
    d = "2-6"
  }
}

variable "stop_time" {
  default = {
    h = 17
    m = 0
    d = "*"
  }
  
}


locals {


  tagsv = [
    for tag in var.tags : tag.value
  ]
  tagsk = [
    for tag in var.tags : tag.key
  ]

  tags = zipmap(local.tagsk, local.tagsv)
}



