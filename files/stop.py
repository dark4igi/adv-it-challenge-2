import json
import os
import boto3

def lambda_handler(event, context):
    filters = []
    tags = os.environ['tags']
    dict = json.loads(tags)
    for i in dict :
        k = i.keys()
        #print (list(k)[0])
        v = i.values()
        #print (list(v)[0])
        filters.append ({
            'Name': 'tag:'+list(k)[0],
                 'Values': [list(v)[0]],
            },)
    client = boto3.client('ec2')
    resp = client.describe_instances(
        Filters = filters
    )
    instances = []
    for i in resp['Reservations']:
        instance = (i['Instances'][0]['InstanceId'])
        instances.append (instance)
    rezlist = []
    for instance in instances:
        rez=client.stop_instances(InstanceIds=[instance], DryRun=False)
        rezlist.append (rez)
    return rezlist